module.exports = function (gulp, pkg, bs, onError, isProduction) {

  var dest = isProduction ? pkg.paths.dist : pkg.paths.tmp;

  return function () {
    return gulp.src( pkg.paths.fonts + '**/*')
      .pipe(gulp.dest( dest + '/assets/fonts'))
      .pipe( bs.reload({stream:true})  );
  }
};