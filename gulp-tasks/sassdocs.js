module.exports = function (gulp, pkg, bs, onError, isProduction) {

  var dest = isProduction ? pkg.paths.dist : pkg.paths.tmp;

  return function () {

    var sassdoc = require('sassdoc');

    var sassdocOptions = {
      dest: dest + '/sassdoc',
      display: {
        access: ['public', 'private'],
        alias: true,
        watermark: true,
      }
    };

    return gulp.src( pkg.paths.styles + '**/*.scss')
      .pipe(sassdoc(sassdocOptions))

      // Release the pressure back and trigger flowing mode (drain)
      // See: http://sassdoc.com/gulp/#drain-event
      .resume();
  }
};