module.exports = function (gulp, pkg, bs, onError, isProduction) {

  var dest = isProduction ? pkg.paths.dist : pkg.paths.tmp;

  var onSassError = function (error) {
    error['plugin'] = 'gulp-sass';
    onError(error);
    this.emit('end');
  };

  return function () {

    var sass = require('gulp-sass'),
        plumber = require('gulp-plumber'),
        sourcemaps = require('gulp-sourcemaps'),
        autoprefixer = require('gulp-autoprefixer'),
        cleanCSS = require('gulp-clean-css'),
        gulpif = require('gulp-if'),
        sassdoc = require('sassdoc');

    var autoprefixerOptions = isProduction ?
      {
        browsers: ['last 3 versions', '> 5%', 'Firefox ESR']
      }
      :
      {
        browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
      };

    var sassdocOptions = {
      dest: dest + '/sassdoc'
    };

    return gulp.src( pkg.paths.styles + '**/*.scss')
      .pipe(plumber({
        errorHandler: onSassError
      }))
      .pipe( gulpif(!isProduction, sourcemaps.init()) )
      .pipe( sass({ precision: 8 }).on('error', onSassError) )
      .pipe( gulpif(!isProduction, sourcemaps.write()) )
      .pipe(autoprefixer(autoprefixerOptions))
      .pipe( gulpif(isProduction, cleanCSS({ compatibility: 'ie9' })) )
      .pipe( gulp.dest( dest + '/assets/styles') )
      .pipe( bs.reload({stream:true}) );
  }
};