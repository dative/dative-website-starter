module.exports = function (gulp, pkg, bs, onError, isProduction) {

  var dest = isProduction ? pkg.paths.dist : pkg.paths.tmp;

  return function () {

    var gulpif = require('gulp-if'),
        plumber = require('gulp-plumber'),
        sourcemaps = require('gulp-sourcemaps'),
        uglify = require('gulp-uglify'),
        babel = require('gulp-babel');

    return gulp.src( pkg.paths.scripts + '**/*.js')
      .pipe(plumber({
        errorHandler: onError
      }))
      .pipe( gulpif(!isProduction, sourcemaps.init()) )
      .pipe( babel({ presets: ['es2015'] }))
      .pipe( gulpif(!isProduction, sourcemaps.write()) )
      .pipe( gulpif(isProduction, uglify()) )
      .pipe( gulp.dest( dest + '/assets/scripts') )
      .pipe( bs.reload({stream:true})  );
  }
};