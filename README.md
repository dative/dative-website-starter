
# <%= appName %>

### Client: <%= clientName %>

Welcome to Dative's website prototype starter, a Node/Gulp based tool that help us focus on building great websites, while handling compilation and optmization.

This tool is built on NodeJS, [Gulp](http://gulpjs.com/) and [Bower](https://bower.io/), and it uses [TWIG](http://twig.sensiolabs.org/) for templating, [SASS](http://sass-lang.com/) for stylesheet, [Babel](https://babeljs.io/) (ES2015) for javascript, [ImageMin](https://github.com/imagemin/imagemin) of image optmization and [BrowserSync](https://browsersync.io/) for a smooth development environment with live reload.

### Requirements:

This is a Node heavy tool, so you need to install [Node](https://nodejs.org/en/), everything else needed will be installed using NPM (Node's package manager).

Once installed, just download or clone this repo locally and, in the terminal type `npm install`. That will install all node dependencies and run `bower install` for some basic libraries, like Twitter Bootstrap and Animate CSS.

### Usage

Once everything is installed, developing is a breeze. Just run `npm run watch` in the terminal from inside you project directory and your browser should open the index page. From there you can start coding the source files.

### Vendor Overrides

This tools makes use of the [bower-main](https://github.com/frodefi/bower-main) library to generate the vendor files. It will load the packages under `dependencies`, in order, by looking inside the package's `bower.json` file under `main` property. Unfortunately not all packages set their `main` inside their `bower.json` file, or sometimes you want to add a non-bower repository to it. For those cases, just set the `overrides` property to your bower file in the following manner:

```json
{
  ...
  "dependencies": {
    "bower-package-name": "*"
  },
  "overrides": {
    "bower-package-name": {
      "main": {
        "development": "file.js",
        "production": "file.min.js"
      }
    }
  }
}
```
[view reference](https://github.com/ck86/main-bower-files#overrides-options)