module.exports = function (gulp, pkg, bs, onError, isProduction) {

  var dest = isProduction ? pkg.paths.dist : pkg.paths.tmp;

  return function () {

    var gulpif = require('gulp-if'),
        imagemin = require('gulp-imagemin'),
        cache = require('gulp-cache');

    return gulp.src( pkg.paths.images + '**/*')
      .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
      .pipe(gulp.dest( dest + '/assets/images'))
      .pipe( bs.reload({stream:true})  );
  }
};