module.exports = function (gulp, pkg, bs, onError, isProduction) {

  var path = require('path');

  var dest = isProduction ? pkg.paths.dist : pkg.paths.tmp;

  var globalsPath = '../' + pkg.paths.src + '/globals.json';
  var globalsProdPath = '../' + pkg.paths.src + '/globals.prod.json';

  var globals = isProduction ? Object.assign({}, require(globalsPath), require(globalsProdPath)) : require(globalsPath);

  var twigExceptionCount = 0;

  var twigExtend = function (Twig) {
    // Limit to error count to 1
    Twig.log.error = function() {
      twigExceptionCount++;
      if (twigExceptionCount == 1) {
        console.log(twigExceptionCount, Array.prototype.slice.call(arguments));
      }
      return;
    }
  };

  var onTwigError = function (error) {
    error['plugin'] = 'gulp-twig';
    onError(error);
  };

  /**
   * getJsonData function
   * read the data directory for json files and return objects
   */
  var getJsonData = function (file, callback) {
    var fs = require('fs');
    var fm = require('front-matter');
    var glob = require('glob');

    var fmData = fm(String(file.contents));

    var merged = Object.assign({}, globals, fmData.attributes);

    glob(pkg.paths.src + '/data/*.json', {}, function (err, files) {
      var data = merged;
      if (files.length) {
        files.forEach( (fPath) => {
          var baseName = path.basename(fPath, '.json');
          data[baseName] = JSON.parse(fs.readFileSync(fPath));
        })
      }
      callback(undefined, data);
    });
  };

  return function () {
    var data = require('gulp-data'),
        flatmap = require('gulp-flatmap'),
        htmlmin = require('gulp-htmlmin'),
        htmlpretty = require('gulp-html-prettify'),
        twig = require('gulp-twig');

    return gulp.src(pkg.paths.templates + '**/[^_]*.twig') // templates that don't start with underscore
          .pipe( data(getJsonData) )
          .pipe( flatmap( (stream, file) => {
            return stream.pipe(twig({
              base: path.join(__dirname, '/../src/templates'),
              onError: onTwigError,
              functions: [
                {
                  name: "pageSegment",
                  func: function (segment) {
                    var relative = path.relative(path.join(__dirname, '/../src/templates'), file.path);
                    var seg = '/' + path.join(path.dirname(relative), path.basename(relative, '.twig'));
                    seg = seg.replace("/index", "");
                    if (seg === "") {
                      seg = "/";
                    }
                    return segment == seg;
                  }
                }
              ],
              filters: [
                {
                  name: "trailingTitle",
                  func: function (value) {
                    return value + ' | ' + globals.siteName;
                  }
                }
              ]
            }));
          }))
          .pipe(htmlmin({collapseWhitespace: true}))
          .pipe(htmlpretty({indent_char: ' ', indent_size: 2}))
          .pipe(gulp.dest(dest))
          .pipe(bs.reload({stream:true}) );
  }
};