module.exports = function (gulp, pkg, bs, onError, isProduction) {

  var dest = isProduction ? pkg.paths.dist : pkg.paths.tmp;

  function browserSyncInit (baseDir, files) {
    bs.use(require('bs-html-injector'), files);
    bs.init(files, {
      startPath: '/',
      port: 8000,
      notify: true,
      logPrefix: 'DativeKit',
      server: {
        baseDir: baseDir,
        serveStaticOptions: {
          extensions: ['html']
        }
      }
    });
  };

  return function () {

    browserSyncInit([dest, pkg.paths.src], [
      dest + '/**/*.css',
      dest + '/**/*.js',
      dest + '/**/*.html'
    ]);

  }
};