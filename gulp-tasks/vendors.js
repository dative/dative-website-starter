module.exports = function (gulp, pkg, bs, onError, isProduction) {

  var dest = isProduction ? pkg.paths.dist : pkg.paths.tmp;

  return function () {

    var concat = require('gulp-concat'),
        uglify = require('gulp-uglify'),
        merge2 = require('merge2'),
        bowerMain = require('bower-main');


    var bowerMainJavaScriptFiles = bowerMain('js','min.js');

    return merge2(
        gulp.src(bowerMainJavaScriptFiles.minified),
        gulp.src(bowerMainJavaScriptFiles.minifiedNotFound)
          .pipe(concat('tmp.min.js'))
          .pipe(uglify())
      )
      .pipe(concat('vendor-scripts.min.js'))
      .pipe( gulp.dest( dest + '/assets/vendors') )
      .pipe( bs.reload({stream:true}) );
  }
};