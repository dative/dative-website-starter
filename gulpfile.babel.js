'use strict';

import path from 'path';
import gulp from 'gulp';
import fs from 'fs';
import pkg from './package.json';
import sequence from 'run-sequence';
import browserSync from 'browser-sync';
import gutil from 'gulp-util';
import chalk from 'chalk';
import notify from 'gulp-notify';
import Console from 'better-console';
import Yargs from 'yargs';

let bs = browserSync.create();
let isProduction = !!(Yargs.argv.production);

/**
 * utility function that takes in an error, makes the OS
 * beep and notifies user
 */
let onError = function (error) {
  console.log(error);
  let lineNumber = (error.line) ? 'LINE ' + error.line + ' -- ' : '';
  let msg = '';
  switch (error.plugin) {
    case 'gulp-twig':
      msg = error.message
      break;
    case 'gulp-sass':
      msg = error.messageOriginal;
      break;
    default:
      msg = 'Unkown error, check gulp file.'
      break;
  }
  return notify({
    title:    'Dative Builder',
    subtitle: 'Task Failed [' + error.plugin + ']',
    message:  msg,
    icon: path.join(__dirname, 'icon-error.png'),
    sound:    'Sosumi'
  })
  .write(error);
};

/**
 * Notify task completion
 */
let taskNotification = function (event) {
  return notify({
    title:    'Dative Builder',
    subtitle: 'Update Successfully',
    message:  'File .' + event.path.replace( __dirname, "") + ' was ' + event.type + ', running tasks...',
    icon:     path.join(__dirname, 'icon-success.png'),
    sound:    false
  })
  .write(event);
};

/**
 * Tasks
 */
gulp.task('styles', require('./gulp-tasks/styles')(gulp, pkg, bs, onError, isProduction) );
gulp.task('scripts', require('./gulp-tasks/scripts')(gulp, pkg, bs, onError, isProduction) );
gulp.task('vendors', require('./gulp-tasks/vendors')(gulp, pkg, bs, onError, isProduction) );
gulp.task('images', require('./gulp-tasks/images')(gulp, pkg, bs, onError, isProduction) );
gulp.task('fonts', require('./gulp-tasks/fonts')(gulp, pkg, bs, onError, isProduction) );
gulp.task('twig', require('./gulp-tasks/twig')(gulp, pkg, bs, onError, isProduction) );
gulp.task('sassdocs', require('./gulp-tasks/sassdocs')(gulp, pkg, bs, onError, isProduction) );
gulp.task('bs-server', require('./gulp-tasks/server')(gulp, pkg, bs, onError, isProduction) );

/**
 * Delete ./dist directory
 */
gulp.task('clean', () => {
  var del = require('del');
  return del([pkg.paths.tmp, pkg.paths.dist]);
});

/**
 * Array of tasks, in order, for complete builds
 * @type {Array}
 */
const buildTasks = [
  // clean up ./dist directory
  'clean',

  // assets
  ['styles', 'scripts', 'vendors', 'images', 'fonts'],

  // template task
  'twig'
];

/**
 * Build tasks
 */
gulp.task('build', (callback) => {
  sequence.apply(null, buildTasks);
  callback();
});

/**
 * Run tasks in sequence to prepare for the watch task
 */
gulp.task('watch:build', (callback) => {
  var tasks = [];
  Array.prototype.push.apply(tasks, buildTasks);
  Array.prototype.push.apply(tasks, ['bs-server']);
  sequence.apply(null, tasks);
  callback();
});

gulp.task('critical', () => {

  var critical = require('critical').stream;

  return gulp.src( pkg.paths.dist + '/**/*.html')
        .pipe(critical({
          base: pkg.paths.dist + '/',
          inline: true,
          css: [
            'dist/assets/styles/main.css'
          ]
        }))
        .on('error', onError)
        .pipe(gulp.dest(pkg.paths.dist));

});

gulp.task('serve', ['watch:build'], () => {
  gulp.watch(pkg.paths.styles + '**/*.scss', ['styles']).on('change', taskNotification);
  gulp.watch(pkg.paths.scripts + '**/*.js', ['scripts']).on('change', taskNotification);
  gulp.watch(pkg.paths.images + '**/*', ['images']).on('change', taskNotification);
  gulp.watch(pkg.paths.fonts + '**/*', ['fonts']).on('change', taskNotification);
  gulp.watch(['src/templates/**/*.twig', 'src/data/*.json'], ['twig']);
});

gulp.task('default', ['serve']);